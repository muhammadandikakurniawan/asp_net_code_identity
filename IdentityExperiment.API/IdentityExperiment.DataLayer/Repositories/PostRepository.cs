﻿using IdentityExperiment.DataLayer.Context;
using IdentityExperiment.DataLayer.Repositories.Interfaces;
using IdentityExperiment.Model.Entities;
using IdentityExperiment.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityExperiment.DataLayer.Repositories
{
    public class PostRepository : IPostRepository
    {
        private ApplicationDbContext _dbContext;
        public PostRepository(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }
        public EFResultModel<Post> Create(Post model)
        {
            EFResultModel<Post> result = new EFResultModel<Post>();
            try
            {

                this._dbContext.Post.Add(model);
                this._dbContext.SaveChanges();

            }
            catch(Exception ex)
            {
                result.IsSuccess = false;
                result.ErrorMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return result;
        }
    }
}
