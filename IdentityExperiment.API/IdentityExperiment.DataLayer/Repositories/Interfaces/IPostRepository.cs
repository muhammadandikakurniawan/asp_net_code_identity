﻿using IdentityExperiment.Model.Entities;
using IdentityExperiment.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityExperiment.DataLayer.Repositories.Interfaces
{
    public interface IPostRepository
    {
        EFResultModel<Post> Create(Post model);
    }
}
