﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using IdentityExperiment.DataLayer.Repositories.Interfaces;
using Newtonsoft.Json;
using IdentityExperiment.Model.Entities;
using IdentityExperiment.Model.Models;

namespace IdentityExperiment.RabbitMqConsumer.Streams
{

    public class PostStream : IPostStream, IDisposable
    {
        private Subject<string> _createPostSubject;
        private IDictionary _createPostsubscribers;
        private readonly IPostRepository _postRepository;
        public PostStream(IPostRepository postRepository)
        {
            this._createPostSubject = new Subject<string>();
            this._createPostsubscribers = new Dictionary<string, object>();
            this._postRepository = postRepository;
        }

        public void PublishCreate(string msg)
        {
            this._createPostSubject.OnNext(msg);
        }

        public void SubcribeCreate(string name)
        {
            if (!this._createPostsubscribers.Contains(name))
            {
                this._createPostsubscribers.Add(name, this._createPostSubject.Where(m => !string.IsNullOrEmpty(m)).Subscribe<string>((data) => {

                    var paramModel = JsonConvert.DeserializeObject<CreatePostModel>(data);

                    var dataInsert = new Post();
                    dataInsert.Image = paramModel.Image;
                    dataInsert.Caption = paramModel.Caption;
                    dataInsert.UserID = paramModel.UserId;
                    dataInsert.ID = Guid.NewGuid().ToString();

                    var res = _postRepository.Create(dataInsert);

                    var dbg = "";
                }));
            }
        }

        public void Dispose()
        {
            if(this._createPostSubject != null)
            {
                this._createPostSubject.Dispose();
            }
            foreach(var s in this._createPostsubscribers)
            {

            }
        }
    }
}
