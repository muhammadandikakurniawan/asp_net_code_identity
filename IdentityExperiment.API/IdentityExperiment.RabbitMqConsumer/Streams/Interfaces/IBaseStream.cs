﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityExperiment.RabbitMqConsumer.Streams
{
    public interface IBaseStream
    {
        void Publish(string msg);
        void subscribe(string name, Action<string> action);
    }
}
