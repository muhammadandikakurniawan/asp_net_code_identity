﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityExperiment.RabbitMqConsumer.Streams
{
    public interface IPostStream
    {
        void SubcribeCreate(string name);
        void PublishCreate(string msg);
    }
}
