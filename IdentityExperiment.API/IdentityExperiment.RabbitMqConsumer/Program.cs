﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client.Events;
using System.Threading.Tasks;
using IdentityExperiment.RabbitMqConsumer.Consumers;
using Microsoft.Extensions.DependencyInjection;
using IdentityExperiment.DataLayer.Context;
using Microsoft.EntityFrameworkCore;
using IdentityExperiment.Service.Interfaces;
using IdentityExperiment.Service.Implements;
using IdentityExperiment.DataLayer.Repositories.Interfaces;
using IdentityExperiment.DataLayer.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.IO;
using IdentityExperiment.Model.ApplicationContextModel;
using IdentityExperiment.RabbitMqConsumer.Streams;

namespace IdentityExperiment.RabbitMqConsumer
{
    class Program
    {

        public static IConfiguration Configuration;
        static void Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();
            Configuration = (new ConfigurationBuilder())
           .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
           .AddJsonFile("appsettings.json", true,true)
           .Build();

            string connString = Configuration.GetConnectionString("DefaultConnection");

            //===================================== repositories ================================================
            services.AddDbContext<ApplicationDbContext>(options => options.UseMySql(Configuration.GetConnectionString("DefaultConnection")),
                ServiceLifetime.Transient
             );

            services.AddScoped<IPostRepository, PostRepository>();
            //===================================== end repositories ============================================

            

            //configure for read app setting
            services.Configure<AppConfigModel>(opt => opt = Configuration.GetSection("AppConfig").Get<AppConfigModel>());

            ////===================================== consumers ===================================================
            services.AddScoped<IPostConsumer, PostConsumer>();
            ////===================================== end consumers ===============================================

            ////===================================== streams =====================================================
            services.AddTransient<IPostStream, PostStream>();
            ////===================================== end streams =================================================



            IServiceProvider serviceProvider = services.BuildServiceProvider();
            serviceProvider.GetService<IPostConsumer>().CreatePost();






            Console.ReadKey();
        }
    }
}
