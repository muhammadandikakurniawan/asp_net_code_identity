﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client.Events;
using System.Threading.Tasks;
using IdentityExperiment.RabbitMqConsumer.Streams;

namespace IdentityExperiment.RabbitMqConsumer.Consumers
{
    public class PostConsumer : IPostConsumer
    {
        private readonly IPostStream _postStream;
        public PostConsumer(IPostStream postStream)
        {
            _postStream = postStream;
        }
        public void CreatePost()
        {
            string exchange = "PostExchange.Create";
            string queue = "PostQueue.Create";

            _postStream.SubcribeCreate(Guid.NewGuid().ToString());

            BaseConsumer.Listen(_postStream.PublishCreate, queue, exchange);

            

            var factory = new ConnectionFactory()
            {
                HostName = "192.168.99.100",
                UserName = "guest",
                Password = "guest",
            };

        }

    }
}
