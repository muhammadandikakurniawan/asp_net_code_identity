﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityExperiment.RabbitMqConsumer.Consumers
{
    public interface IPostConsumer
    {
        void CreatePost();
    }
}
