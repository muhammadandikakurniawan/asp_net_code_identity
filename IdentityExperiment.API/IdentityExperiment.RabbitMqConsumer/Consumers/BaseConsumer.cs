﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client.Events;
using System.Threading.Tasks;
using IdentityExperiment.RabbitMqConsumer.Streams;

namespace IdentityExperiment.RabbitMqConsumer.Consumers
{
    public class BaseConsumer
    {
        public static void Listen(Action<string> publisher, string queue, string exchange)
        {
            var factory = new ConnectionFactory()
            {
                HostName = "192.168.99.100",
                UserName = "guest",
                Password = "guest",
            };

            using (var connection = factory.CreateConnection())
            {
                using(var channel = connection.CreateModel())
                {

                    channel.ExchangeDeclare(exchange: exchange, type: ExchangeType.Fanout);

                    channel.QueueDeclare(queue: queue, exclusive: false);

                    channel.QueueBind(queue: queue, exchange: exchange, routingKey:"");

                    var consumer = new EventingBasicConsumer(channel);

                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);

                        publisher(message);

                        Console.WriteLine(" [PostQueue.Create_node1] {0}", message);
                    };

                    channel.BasicConsume(queue: queue, autoAck:true, consumer: consumer);

                    Console.WriteLine(" Press [enter] to exit.");
                    Console.ReadLine();
                }
            }
        }

    }
}
