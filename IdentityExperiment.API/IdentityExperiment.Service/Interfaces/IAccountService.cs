﻿using IdentityExperiment.Model.ApplicationContextModel;
using IdentityExperiment.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IdentityExperiment.Service.Interfaces
{
    public interface IAccountService
    {
        Task<ResultModel<RegisterResultModel>> RegisterAsync(RegisterViewModel param);
        Task<ResultModel<RegisterResultModel>> LoginAsync(LoginViewModel param);
    }
}
