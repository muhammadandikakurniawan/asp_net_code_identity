﻿using IdentityExperiment.Model.ApplicationContextModel;
using IdentityExperiment.Model.Entities;
using IdentityExperiment.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IdentityExperiment.Service.Interfaces
{
    public interface IPostService
    {
        Task<EFResultModel<Post>> ConsumeMsPostAsync(string param);
        Task PublishMsPostCreateAsync(CreatePostModel param);
    }
}
