﻿using IdentityExperiment.Model.ApplicationContextModel;
using IdentityExperiment.Model.Models;
using IdentityExperiment.Service.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;

namespace IdentityExperiment.Service.Implements
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IOptions<AppConfigModel> _config;
        public AccountService(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser>  signInManager,
            IOptions<AppConfigModel> config
        )
        {
            this._userManager = userManager;
            this._signInManager = signInManager;
            this._config = config;
        }

        public async Task<ResultModel<RegisterResultModel>> LoginAsync(LoginViewModel param)
        {
            ResultModel<RegisterResultModel> result = new ResultModel<RegisterResultModel>();
            try
            {

                var loginRes = await _signInManager.PasswordSignInAsync(
                    userName:param.UserName, 
                    password:param.Password,
                    isPersistent:true,
                    lockoutOnFailure:false
                );
                if (!loginRes.Succeeded)
                {
                    result.Modify("11", $"User not found", false, null);
                    return result;
                }

                var userToverify = await _userManager.FindByNameAsync(param.UserName);
                if (userToverify == null)
                {
                    result.Modify("11", $"User not found", false, null);
                    return result;
                }
                var roles = await _userManager.GetRolesAsync(userToverify);
                //var identityClaims = await _userManager.GetClaimsAsync(userToverify);

                List<Claim> identityClaims = new List<Claim>();
                identityClaims.Add(new Claim(ClaimTypes.NameIdentifier, userToverify.UserName));
                roles.ToList().ForEach(role =>
                {
                    identityClaims.Add(new Claim(ClaimTypes.Role, role));
                });
                
                identityClaims.Add(new Claim(ClaimTypes.Email, userToverify.Email));

                //create token process
                var key = Encoding.ASCII.GetBytes(this._config.Value.JwtConfig.Key);
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenExpiredDate = DateTime.Now.AddMinutes(15);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(identityClaims),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                    Audience = this._config.Value.JwtConfig.Issuer,
                    Issuer = this._config.Value.JwtConfig.Issuer,
                    Expires = tokenExpiredDate
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);

                string tokenString = tokenHandler.WriteToken(token);

                RegisterResultModel resultValue = new RegisterResultModel()
                {
                    Username = param.UserName,
                    AccessToken = tokenString,
                    TokenExpiredDate = tokenExpiredDate
                };

                result.Modify("00", "Login Success", true, resultValue);
            }
            catch (Exception ex)
            {
                result.SetError(ex);
            }
            return result;
        }
        public async Task<ResultModel<RegisterResultModel>> RegisterAsync(RegisterViewModel param)
        {
            ResultModel<RegisterResultModel> result = new ResultModel<RegisterResultModel>();
            try
            {
                var user = new ApplicationUser {
                    UserName = param.UserName,
                    Email = param.Email,
                };
                var registerRes = await _userManager.CreateAsync(user, param.Password);
                if (!registerRes.Succeeded)
                {
                    var listError = registerRes.Errors.ToList().Select(x => x.Description);
                    string errorStr = string.Join(',', listError);
                    result.Modify("10", $"Register failed : {errorStr}", false, null);
                    return result;
                }

                var userToverify = await _userManager.FindByNameAsync(param.UserName);
                if (userToverify == null)
                {
                    result.Modify("11", $"User not found", false, null);
                    return result;
                }

                var setRoleRes = await _userManager.AddToRoleAsync(userToverify, param.Role);
                if (!setRoleRes.Succeeded)
                {
                    var listError = setRoleRes.Errors.ToList().Select(x => x.Description);
                    string errorStr = string.Join(',', listError);
                    result.Modify("12", $"Register failed : {errorStr}", false, null);
                    return result;
                }

                var roles = await _userManager.GetRolesAsync(userToverify);

                var identityClaims = await _userManager.GetClaimsAsync(userToverify);
                identityClaims.Add(new Claim(ClaimTypes.NameIdentifier, userToverify.UserName));
                roles.ToList().ForEach(role =>
                {
                    identityClaims.Add(new Claim(ClaimTypes.Role, role));
                });

                identityClaims.Add(new Claim(ClaimTypes.Email, userToverify.Email));

                //create token process
                var key = Encoding.ASCII.GetBytes("identityprojectseretkey");
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenExpiredDate = DateTime.Now.AddMinutes(15);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(identityClaims),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                    Audience = "identityproject",
                    Issuer = "identityproject",
                    Expires = tokenExpiredDate
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);

                string tokenString = tokenHandler.WriteToken(token);

                RegisterResultModel resultValue = new RegisterResultModel()
                {
                    Username = param.UserName,
                    AccessToken = tokenString,
                    TokenExpiredDate = tokenExpiredDate
                };

                result.Modify("00", "Register Success", true, resultValue);
            }
            catch(Exception ex)
            {
                result.SetError(ex);
            }
            return result;
        }
    }
}
