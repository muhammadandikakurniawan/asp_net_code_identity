﻿using IdentityExperiment.Model.ApplicationContextModel;
using IdentityExperiment.Model.Models;
using IdentityExperiment.Service.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;
using IdentityExperiment.Common.Helpers;
using Newtonsoft.Json;
using IdentityExperiment.DataLayer.Context;
using IdentityExperiment.DataLayer.Repositories.Interfaces;
using IdentityExperiment.Model.Entities;

namespace IdentityExperiment.Service.Implements
{
    public class PostService : IPostService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        //private readonly IOptions<AppConfigModel> _config;
        private readonly IPostRepository _postRepository;
        public PostService(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser>  signInManager,
            //IOptions<AppConfigModel> config,
            IPostRepository postRepository
        )
        {
            this._userManager = userManager;
            this._signInManager = signInManager;
            //this._config = config;
            this._postRepository = postRepository;
        }

        public async Task<EFResultModel<Post>> ConsumeMsPostAsync(string param)
        {
            EFResultModel<Post> result = new EFResultModel<Post>();
            try
            {
                var paramModel = JsonConvert.DeserializeObject<CreatePostModel>(param);

                var dataInsert = new Post();
                dataInsert.Image = paramModel.Image;
                dataInsert.Caption = paramModel.Caption;
                dataInsert.UserID = paramModel.UserId;

                return _postRepository.Create(dataInsert);

            }
            catch(Exception ex)
            {

            }

            return result;
        }

        public async Task PublishMsPostCreateAsync(CreatePostModel param)
        {
            ResultModel<CreatePostResultModel> result = new ResultModel<CreatePostResultModel>();
            result.Modify("00", "", true, null);
            try
            {
                //await Task.Run(() =>
                //{
                //    RabbitMqHelper.Publish(
                //    this._config.Value.RabbitMqConfig.Host,
                //    this._config.Value.RabbitMqConfig.Username,
                //    this._config.Value.RabbitMqConfig.Password,
                //    "PostExchange.Create",
                //    JsonConvert.SerializeObject(param)
                //);
                //});

                await Task.Run(() =>
                {
                    RabbitMqHelper.Publish(
                    "192.168.99.100",
                    "guest",
                    "guest",
                    "PostExchange.Create",
                    JsonConvert.SerializeObject(param)
                );
                });
            }
            catch (Exception ex)
            {
                result.SetError(ex);
            }
        }
    }
}
