﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityExperiment.Model.ApplicationContextModel;
using IdentityExperiment.Model.Models;
using IdentityExperiment.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IdentityExperiment.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private IAccountService _accountService;
        
        public AccountController(
            IAccountService accountService
        )
        {
            this._accountService = accountService;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<ResultModel<RegisterResultModel>> Register([FromBody] RegisterViewModel param)
        {
            return await this._accountService.RegisterAsync(param);
        }

        [HttpPost]
        [Route("Login")]
        public async Task<ResultModel<RegisterResultModel>> Login([FromBody] LoginViewModel param)
        {
            return await this._accountService.LoginAsync(param);
        }

        [HttpGet]
        public IActionResult TestAuthorization()
        {
            return Ok();
        }
    }
}
