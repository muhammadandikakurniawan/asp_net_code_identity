﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityExperiment.Model.ApplicationContextModel;
using IdentityExperiment.Model.Models;
using IdentityExperiment.Service.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IdentityExperiment.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RoleController : ControllerBase
    {
        private readonly RoleManager<IdentityRole> _roleManager;

        public RoleController(
            RoleManager<IdentityRole> roleManager
        )
        {
            this._roleManager = roleManager;
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add([FromBody] AddRoleViewModel param)
        {
            var objectRes = new Dictionary<string,object>{
                {"StatusCode", "00" },
                {"StatusMessage", "Success" }
            };
            bool isExist = await _roleManager.RoleExistsAsync(param.Name);
            if (isExist)
            {
                objectRes["StatusCode"] = "10";
                objectRes["StatusMessage"] = "Role already exist";
                return Ok(objectRes);
            }

            var role = new IdentityRole();
            role.Name = param.Name;

            var createRes = await _roleManager.CreateAsync(role);
            if (!createRes.Succeeded)
            {
                objectRes["StatusCode"] = "11";
                objectRes["StatusMessage"] = "Create role failed";
                return Ok(objectRes);
            }

            return Ok(objectRes);
        }
    }
}
