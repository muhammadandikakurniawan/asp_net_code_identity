﻿using IdentityExperiment.Common.Helpers;
using System;
using System.Threading;

namespace IdentityExperiment.Common
{
    class Program
    {
        static void Main(string[] args)
        {
            for(int i = 10; i >= 1; i--)
            {
                RabbitMqHelper.Publish("192.168.99.100", "guest", "guest", "PostExchange.Create", "{'message':'hy, this is message', 'index':{"+i+"}}");
                Thread.Sleep(3000);
            }
            Console.WriteLine("Hello World!");
        }
    }
}
