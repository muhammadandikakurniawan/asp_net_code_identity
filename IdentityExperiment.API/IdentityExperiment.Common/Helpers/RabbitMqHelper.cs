﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityExperiment.Common.Helpers
{
    public class RabbitMqHelper
    {
        public static void Publish(string host, string username, string password, string exchangeName, string payload)
        {
            var factory = new ConnectionFactory()
            {
                HostName = host,
                UserName = username,
                Password = password
            };

            var body = Encoding.UTF8.GetBytes(payload);

            using (var connection = factory.CreateConnection())
            {
                using(var channel = connection.CreateModel())
                {
                    //create exchange
                    channel.ExchangeDeclare(exchange: exchangeName, type: ExchangeType.Fanout);

                    //channel.ExchangeBind(routingKey:routeKey,exc)

                    channel.BasicPublish(
                        exchange: exchangeName,
                        routingKey: "",
                        basicProperties: null,
                        body: body
                    );
                }
            }
        }
    }
}
