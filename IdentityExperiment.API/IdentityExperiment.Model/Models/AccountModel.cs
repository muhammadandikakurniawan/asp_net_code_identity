﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityExperiment.Model.Models
{
    public class RegisterResultModel
    {
        public string AccessToken { get; set; }
        public string Username { get; set; }
        public DateTime TokenExpiredDate { get; set; }
    }
}
