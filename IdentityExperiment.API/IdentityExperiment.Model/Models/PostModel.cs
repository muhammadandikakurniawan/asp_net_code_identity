﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityExperiment.Model.Models
{
    public class PostModel
    {
    }

    public class CreatePostModel
    {
        public string Caption { get; set; }
        public string Image { get; set; }
        public string UserId { get; set; }
    }

    public class CreatePostResultModel
    {
        public CreatePostModel Data { get; set; }
        public string IsSuccess { get; set; }
    }
}
