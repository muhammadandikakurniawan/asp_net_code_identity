﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityExperiment.Model.Models
{
    public class ResultModel<T>
    {
        public ResultModel()
        {
            this.Success = true;
        }
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public bool Success { get; set; }
        public T Value { get; set; }

        public void Modify(string statusCode, string statusMessage, bool success, T value)
        {
            this.StatusCode = statusCode;
            this.StatusMessage = statusMessage;
            this.Value = value;
            this.Success = success;
        }

        public void SetError(Exception ex)
        {
            this.StatusCode = "500";
            this.StatusMessage = ex.InnerException != null ? $"Internal server error : {ex.InnerException.Message}" : $"Internal server error : {ex.Message}";
            this.Success = false;
        }

    }

    public class EFResultModel<T>
    {
        public EFResultModel()
        {
            this.IsSuccess = true;
        }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public T Data { get; set; }
    }

    public class ResultPageModel<T> : ResultModel<T>
    {
        public ResultPageModel() : base()
        {

        }
        public int TotalPage { get; set; }
        public int TotalData { get; set; }
    }
}
