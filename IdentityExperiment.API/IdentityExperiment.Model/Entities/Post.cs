﻿using IdentityExperiment.Model.ApplicationContextModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IdentityExperiment.Model.Entities
{
    [Table("Post")]
    public class Post
    {
        [Key]
        public string ID { get; set; }

        [Required, ForeignKey("User")]
        public string UserID { get; set; }
        [Required]
        public string Image { get; set; }
        [Required]
        public string Caption { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}
