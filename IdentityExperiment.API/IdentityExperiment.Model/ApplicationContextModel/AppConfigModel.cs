﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IdentityExperiment.Model.ApplicationContextModel
{
    public class AppConfigModel
    {
        public JwtConfigModel JwtConfig { get; set; }
        public RabbitMqConfigModel RabbitMqConfig { get; set; }
    }

    public class JwtConfigModel
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
    }

    public class RabbitMqConfigModel
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
