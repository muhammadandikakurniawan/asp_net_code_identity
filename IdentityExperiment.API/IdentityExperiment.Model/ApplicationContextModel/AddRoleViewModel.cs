﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IdentityExperiment.Model.ApplicationContextModel
{
    public class AddRoleViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}
