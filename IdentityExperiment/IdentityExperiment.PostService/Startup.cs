using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityExperiment.Common.Helpers;
using IdentityExperiment.DataLayer.Context;
using IdentityExperiment.DataLayer.Repositories;
using IdentityExperiment.DataLayer.Repositories.Interfaces;
using IdentityExperiment.Model.ApplicationContextModel;
using IdentityExperiment.PostService.AppConfig;
using IdentityExperiment.Service.Implements;
using IdentityExperiment.Service.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Ocelot.DependencyInjection;
using Ocelot.Provider.Consul;

namespace IdentityExperiment.PostService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //configure for read app setting
            services.Configure<AppConfigModel>(Configuration.GetSection("AppConfig"));
            services.AddControllers();

            //configure consul
            services.AddConsulConfig(Configuration);
            services.AddOcelot().AddConsul();

            //configure database
            services.AddDbContext<ApplicationDbContext>(options => options.UseMySql(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                //.AddSignInManager<ApplicationUser>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // services
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IPostService, IdentityExperiment.Service.Implements.PostService>();

            //repositories
            services.AddScoped<IPostRepository, PostRepository>();

            services.AddHttpClient<IConsulHttpClient, ConsulHttpClient>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseConsul(Configuration);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
