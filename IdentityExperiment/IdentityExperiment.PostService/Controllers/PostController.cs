﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Consul;
using IdentityExperiment.Common.Helpers;
using IdentityExperiment.DataLayer.Repositories.Interfaces;
using IdentityExperiment.Model.Models;
using IdentityExperiment.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IdentityExperiment.PostService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PostController : ControllerBase
    {

        private IConsulHttpClient _consulHttpClient;
        private IPostService _postService;
        public PostController(IConsulHttpClient consulHttpClient, IPostService postService)
        {
            _consulHttpClient = consulHttpClient;
            _postService = postService;
        }
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        [HttpGet]
        public IEnumerable<WeatherForecast> GetPost()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add([FromBody] CreatePostModel param)
        {
            await _postService.PublishMsPostCreateAsync(param);

            return Ok();
        }


        [HttpGet()]
        [Route("GetCustomerBasket")]
        public async Task<IActionResult> GetCustomerBasket()
        {
            //var basket = await _consulHttpClient.GetAsync<object>("AuthService", new Uri("/api/Account/TestAuthorization"));

            //return Ok(basket);

            List<Uri> _serverUrls = new List<Uri>();
            var consulClient = new ConsulClient(c => c.Address = new Uri("http://192.168.99.100:8500"));
            var services = consulClient.Agent.Services().Result.Response;
            foreach (var service in services)
            {
                var isSchoolApi = service.Value.Tags.Any(t => t == "School") &&
                                  service.Value.Tags.Any(t => t == "Students");
                if (isSchoolApi)
                {
                    var serviceUri = new Uri($"{service.Value.Address}:{service.Value.Port}");
                    _serverUrls.Add(serviceUri);
                }
            }

            return Ok(_serverUrls);
        }
    }
}
